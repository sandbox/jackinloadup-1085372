(function ($) {

  $(document).ready(function() {
      // check if modal_checkbox should be checked
      class_array = $(':input[name="class"]').val().split(' ');
      if($.inArray('linkit-modal', class_array) > -1) 
        $(':input[name="modal_checkbox"]').attr('checked', true);

      $(':input[name="modal_checkbox"]').click(function () {
        class_array = $(':input[name="class"]').val().split(' ');
        if($(this).is(':checked')) class_array.push('linkit-modal');
        else class_array.splice($.inArray('linkit-modal', class_array), 1)

        $(':input[name="class"]').val(class_array.join(' '));
      });
  });

})(jQuery);
