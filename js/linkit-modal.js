(function ($) {

  Drupal.behaviors.initLinkitModalLoad = {
    attach: function(context, settings) {
      if(Drupal.behaviors.initColorboxLoad) {
        $('a.linkit-modal', context).each(function(index) {
          $(this).attr('href', $(this).attr('href') + '?iframe&width=500&height=500');
          $(this).addClass('colorbox-load');
        });

        Drupal.behaviors.initColorboxLoad.attach(context, settings);
      }
    }
  };

})(jQuery);//colorbox-load
